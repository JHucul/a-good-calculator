# A Good Calculator

## Ideal
Create a cross-platform calculator app that is fast and straightforward to use on touch screen and keyboard. Additionally can do just about anything concievably necessary for a calculator.

## Installation
Indev

## Support
Here

## Roadmap
- [ ] UI
- [ ] Full Keyboard Support
- [ ] Full Touch Screen Support
- [ ] Basic Operators (+-*/)
- [ ] More Complex Operators (square root, modulus, etcetera)
- [ ] Graphing In Two Dimensions
- [ ] Calculus Operators (integral, derivative, etcetera)
- [ ] Graphing in Three Dimensions
- [ ] Stepwise Computation Printouts (stretch goal)

## Contributing
Not currently open to contribution, constructive criticism or encouragement may be said and will likely be read.

## Project status
Semi-active development
